# Funciones (in)Utiles

Contiene funciones generadas para (por) el análisis de datos de rendimiento y de ensayos forestales de INTA EEA Bella Vista.

##  Instalación

```{r}
install.packages("remotes")
remotes::install_gitlab("gastonk/funciones-inutiles")
```

[crédito for is_outlier](https://stackoverflow.com/users/2572423/jasonaizkalns)


<!---
[Guia](https://cran.r-project.org/doc/manuals/R-exts.html)
--->
